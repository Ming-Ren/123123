# 2020-09-17 #
- - -
* The second lecture was a little bit interesting because of the Ted talk that talk about human progress.
* Althought we heard about a lot of things that there price goes lower and lower, why do people put price on everything and keeps making most of the price higher and higher?
* Sometimes, comunicating is hard. I will try to analyze my everyday conversation and see how to change myself to be a better person in conflicting conversation.

# 2020-09-24 #
- - -
* Fake news are really bad because we tend to believe in everything we saw.
* One of our group member drop the class last week, it feels bad, but we still manage to make some changes in our presentation to make it still acceptable for the two of us.
* With professor's feedback, this time we knew what we've done wrong, it will be easier to make some changes.

# 2020-10-08 #
- - -
* The video's animation is very clear to show how everything works.
* I learned how credit works in marketing.
* We need to see the bigger picture about the market, in order to understand everything.
* I think this video is a mind-blower. Before this video, I didn't know that we are basicly trading our credit.
* There are lots of platform for us to know which news are real, so don't easily believe everything.

# 2020-10-15 #
- - -
* Learned about the development of many other contries economy.
* Truly know what facism is, and how the "love your country" works.
* Money is a fiction story which haunted everyone in our life!
* It's very sad to truly understand that we might not be able to aford a house.

# 2020-10-22 #
- - -
* Getting a cold was not because of cold temperature.
* Fiction stories and myth really affect us a lot, before this class, we don't even think about that.
* After the video of health care, I suddenly realize that we are always making up when we are really sick, but sometimes we didn't even try to prevent it from happening.

# 2020-10-29 #
- - -
* Today we got the super group and it really got me to think about how cool it is to mix everybody's idea in one presentation.
* It is really important to be happy. One of our groups idea of being healthy is to avoid negativity, but it was replaced because of the super group.
* Lot's of people think that it is nonsense for people to always be in depression, but like what said in the video, it's not that easy to get over it.

# 2020-11-05 #
- - -
* Death is unexpected, but very close to all of us, sometimes, we forget how it feels to loss someone that is close to us.
* This is the first time that we start our final project, it is kind of hard because of a lot of information.

# 2020-11-12 #
- - -
* Law is always hard for me to understand, I think it is still important to know a thing or two about it.
* The universal basic income is a good idea, but I really think it is only when everyone wants to do more.
* It is very important to say things, to claim some points when we have evidence.

# 2020-11-19 #
- - -
* The technology that we expected in the future, or exists only in our dream, seems to be a lot closer to us.
* After other people's explanation, the justice system is a lot clearer for me.
* It is difficult to come up with things to help people connect with each other.

# 2020-11-26 #
- - -
* Today, we have an experiment of phones addiction, I think it is very meaningful, but sometimes using phone is inevitable, and it really bothers me if I can't use it to take notes or something.
* This time, our presentation is very good, I think. But the imformation, and the experiments are too much, we don't have the time to finish searching them all.

# 2020-12-03 #
- - -
* I think that the PPT we prepared is a waste when we don't present it, because we still spend an amount of time to do this, and it will let some group lose their chance to go on stage to present.
* It is very interesting that with dfferent viewpoint, the news can tell a totally different story.
* Our world is getting worst by our developement, we should cherish the environment.

# 2020-12-10 #
- - -
* This time, we have a debate, and I think this is a very good way to do our presentation, every group has their chance to present.
* Our group has a small topic about income, and in our group we have two people that is in the group progect of UBI, so we already know some of it.
* 3rd industrial revolution might not be bad, because of the human developing, but we will have to improve in order to survive the change.

# 2020-12-17 #
- - -
* Successful and productive.
* I had a lot of time to think about some final projects so I think this day was a success.
* I will try to get up earlier.

# 2020-12-18 #
- - -
* Unsuccessful and unproductive.
* I didn't sleep, so this day was very terrible.
* I failed to get up early, I will still try to do it.

# 2020-12-19 #
- - -
* Successful and unproductive.
* I failed to get up early, I will still try to do it.

# 2020-12-20 #
- - -
* Successful and unproductive.
* I failed to get up early, I will still try to do it.

# 2020-12-21 #
- - -
* Successful and unproductive.
* I slept the whole day, and it felt good.
* I failed to get up early, I will still try to do it.

# 2020-12-22 #
- - -
* Successful and productive.
* I finish a lot of work, so in total this day was a sucess.
* I failed to get up early, I will still try to do it.

# 2020-12-23 #
- - -
* Unsuccessful and unproductive.
* Because of allergy and the weather changing, I'm not feeling so well.
* I will try to exercise regularly to see if it helps.

# 2020-12-24 #
- - -
* Unsuccessful and unproductive.
* Slept all the way through the day.
* Try to prepare for final and failed.
* I will try to focus more on the book.

# 2020-12-25 #
- - -
* Unsuccessful and unproductive.
* Using more than 6 hours to prepare automatic control project and fail to do it.
* I will try to make some better decision to use my time.

# 2020-12-26 #
- - -
* Successful and productive.
* Our guitar club has the graduation for our new members, and it was epic.
* I done well in preparing for the whole thing.

# 2020-12-27 #
- - -
* Unsuccessful and unproductive.
* The project for automatic control is out of control, we barely have any progress.

# 2020-12-28 #
- - -
* Successful and unproductive.
* I skipped the first class because I slept too late.
* The exam in the afternoon was very hard.
* I failed to get up early, I will try to do it.

# 2020-12-29 #
- - -
* Successful and productive.
* I think that I've done well on the project in fluid dynamic.
* I will try to study more for the exams.

# 2020-12-30 #
- - -
* Successful and unproductive.
* I succesfully get up early and do the final project for Automatic Control, but the project's errors were out of control.
* I have to get up early tommorow, I hope I can do it.

# 2020-12-31 #
- - -
* This week is the final week, I spend a lot of time to prepare all the exams, hope everything will be good.
* While searching for the answers, I know a lot more thing about the world that is actually quite interesting.
* The final project is kind of difficult, but we are working on it.
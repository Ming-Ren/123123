This diary file is written by Edwin Ke in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* Tried to come up with a few mediators, but turns out I was confused with the definition of a mediator.
* Our group didn't get to do the presentation. Next time, we will be faster to grab the chance.
* The layouts of a PowerPoint presentation can really affect its outcome.
* This is my first time using BitBucket. Might take a while to get used to this tool.
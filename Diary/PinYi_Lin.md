This diary file is wriiten by Pin Yi Lin E14056253 in the course Professional skills for engineering the third revolution.

## 2019-09-19

Today, the course is about the conflicts and the speech from the Linux founder.
We realize that the conflicts are not always bad, but we can know more about each other.
In the other terms, the TED speech makes me realize the importance of Resource sharing.
There are huge amount of people tend to make the world better in their own ways, and sometimes doesn't ask for anything back,
I found this quite inspiring.

## 2019-09-26

In the beginning of the class, each group introduce the presentation which are prepared last week.
The topic is to prove whether the claims in Steven Prinker's speech is true or not.
Our group choose the topic of the gap between rich and poor.
We find the statistics with the inequality wealth as evidence. 
In conclusion, we assumed that the gap between rich and poor is decreasing, which makes a better world.
However, after listening others presentation, I found that our information isn't convincing enough,
and the lack of researching ability might be the main reason.
Luckily, at the end of the class, we watch another TED speech about distinguishing external information between 
fake and true. With this video, I think it would help me to improve my research ability. 
In addition, I think it is really important for everyone to have the ability to find the truth with the prosperity of internet.

## 2019-10-03

The topic of this week presentation is to reject or agree the claims which Christiane Amanpour says in TED talks.
In one of her coments, she says that fake news spread as fast as the speed of sound and light. 
However, we find it too exaggerate, and we would like to know more about it.
To look into it, we research for more informaitons. 
According to a research, it says that fake news spread six times faster than truth.
In addition, the research informs that it takes ten hours to spread fake news to 1500 people.
With the informations we get, we can assume that fake news spread so much faster than truth, however it couldn't be as fast as the speed of sound or light.
In terms of the lecture today, we know more about how a bank works and how a economic system work, which students from engineering fields should also understand.
To sum up, I think that the course really leads students to look into our world in different sights.

The topic of presentation is to reject or agree the claims that Christiane Amanpour says in TED talks.
Christiane Amanpour says that fake news spread as fast as the speed of light and sound, however we find it too exaggerate.
To look more into it, we find more informations. 
According to a research, it claims that fake news spread six times faster than truth. 
In addition, the research informs that it takes 10 hours for 1500 people to get the fake news.
With the informations online, we know that fake news spread so much faster than truth, however it couldn't be as fast as the speed of sound or light.
In terms of the lecture today, we know more about how a bank works and how a economic system works, which students from engineerng fields should also understand.
This course is really leading students to look into our world in different sights.

## 2019-10-17

The topic of last week is about economic. 
However, I know very less about it. 
Luckily, I have a partner from department of statistics, 
with his help I am able to know more about how interest rates influences the society. 
I find this a great course, however, I have hard working load this semester, 
and I hope I could attend more during class.

## 2019-10-24

The topic of this week is how to live healthier,
and it not only includes physical but also mental.
However, today professor ask how many people feel stressed in the past few weeks,
the result is quite impressed that only 3 classmates didn't raise their hands,
which means that most of the Taiwanese students feel stressed on studying.

## 2019-10-31
This week we talk about depressions.
Our group choose the claim that says black americans have higher risk of getting depressions.
However, as we look into the issue, we deny it.
We found that, actually, white male Americans have higher population of getting depression.
With this contrary conclusion, we assumed that white male Americans have the best status in the US society,
thus, they might have lower stress prevention or tends to stay silence when they face problems.
The conclusion tells us not to believe in all the informations we get, even it sounds reasonable.
Thinking independently is a important task people face nowadays.

## 2019-11-07

I have school event last week, so I didn't attend the class.
However, to find the rules of having a happy and fulfilling life,
I think keeping hobbies to keep creative and healthy is important.
Also, I believe having a good relationship is also impoetant.
For example, the Taiwanese celebrity Alyssa Chia would give an evidence.
Alyssa Chia has a divorce since 2010, and it was very depressed that she nearly commited suicide.
She dropped out of entertainment circle until 2015 when she met her true love.
Not only she started her career again, but also filmed many famous TV series.
In addition, Alyssa Chia even won Best Actress in Golden Bell Award this year.
Also, she wrote a book to talk about her big turn in life, and how important her husband meant in her life.

## 2019-11-14
In the beginning of the class, 
the class discuss about any externel or internal factors we would like to change individually.
The group I choose is not satisfied with my internal.
We sum up with three factors people would like to change generally, time management, focus and procrastination.
And our classmates suggest us to do meditation or keep active everyday.
The other topic of class is about law.
We are taught to follow the laws, however, school rarely tells us the value or the meaning of laws.
This week we watch videos about strawman law and legal fiction.
Telling us that we should know more about our law to protect our civil rights.

## 2019-11-21
This week we talk about the rapid innovation of technology.
Internet, social network, cellphones have play important roles in our life.
However, with these technology, we have to define how we use it.
Thanks to the inventions, we are in a global society which is easy to contact others.
However, the convenience has bring some of us addiction.
We rely to much onn electronic devices which causes connection between people in reality further, and even brings anxiety.
Thus, it is important for us to find out a solution.

## 2019-11-28
This week we talk about how to stay connected and free with the aim of reaching consensus on minimal and sufficient
set of enforceable laws that safeguards freedom of speech, privacy, equal access, easy sharing, and all other positive things
we can compe up with.

## 2019-12-05
At first, we talk about last week presention homework, which we should analyze news from different countries.
We can learn the culture and the stands of government by looking into the news.
In additon, we also are assigned to categorize what kind of aspect is each country standing for.
I find it quite interesting.
At the second part of the class, we learn about planetary boundaries, which professor has already mentioned
in the beginning of the course.
We learn about several standards to evaluate whether our planet is in danger.
Our group has the topic of novel entities, 
which include all items human beings created in a not natural way.
These things might be pollutants, radioactive or genetic modification products.
Next week, we will try to do more research and know more about this issue.
Also, there will be a class debate which I think will be very interesting, looking forward!

## 2019-12-12
I love the topic of the lecture today.
During the class, each of our group introduce the planetary boundaries and social foundations
which we are assigned.
We need to stand out for our statement, and compete with other groups for which issue is the most important.
I believe most of us have great times, since it is the time when everyone speak,
and I fill interesting when the whole class is connected.
We create some surprising ideas and feedback, forcing us to consider more on our mother planet 
when we are doing something harmful.

## 2019-12-19
Today we have a debate too.
Class members are seperated into three groups, including capitalist, environmentalist and workers.
I choose the role of being environmentalist, and we discuss about wealth inequality.
After the debate, as the perspective of environmental friendly, 
we believe that human is not the only creature living on the earth.
Consequently, we have no right to take natural resources without limit,
instead we should respect every species, and always consider the environmental effect while developing.

## 2019-12-20
successful
I have utilized the day successfully.
Read more tomorrow.

## 2019-12-21
successful
I take TOEFL test today, and have great time with my parents.
Use less cellphone tomorrow.

## 2019-12-22
normal
We have graduate photoshot today, and have race car meeting at night.
I sleep the whole afternoon, since I wake up too early today.
However, we face the race car currently conditions today, and review our schedule this semester again.
Sleep earlier instead of surfing internet.

## 2019-12-23
normal
I skip the class today. However, I clean up my room entirely.
With a cleaner room, I beileve I could be more productive.
Clean up my room a bit every day.

## 2019-12-24
normal
I splash water on my computer, which I spend too much time on fixing it.
Today, the racing car team have a meeting with our advising professor.
The meeting works well, and our professor would help us.
Be careful.

## 2019-12 25
Do extra reading everyday.
Maintain a neat room.
Sleep earlier if allowed.
Keep in touch with your friends.
Stand a regular routine.

## 2019-12-26
Today everyone share their five rules to be more successful and productive every day.
After all the presentation, most of us believe having a regular routine and excersice more 
is the most suggested.
After that, all of the groups present their homework last week, including various topics, 
wealth inequality, economic for example.

## 2020-01-02
Today every group present their three idea solution to three questions they selected.
However, most of us didn't catch the point. 
Instead, we should focus on the solutions that we can do immediately.
Next week will be our final test and project.
Hope that we can do well on the video.